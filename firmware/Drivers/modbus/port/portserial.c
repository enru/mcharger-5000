/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portserial.c,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */

#include "port.h"
#include "stm32l4xx_hal.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "main.h"

/* ----------------------- static functions ---------------------------------*/



UART_HandleTypeDef huart_m;

//static void USART_SetConfig(UART_HandleTypeDef *huart);

/* ----------------------- Start implementation -----------------------------*/
void
vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    /* If xRXEnable enable serial receive interrupts. If xTxENable enable
     * transmitter empty interrupts.
     */
    if(xRxEnable)
    {
        __HAL_UART_ENABLE_IT(&huart_m, UART_IT_RXNE);

        //HAL_GPIO_WritePin(R_W_RS485_GPIO_Port,R_W_RS485_Pin,GPIO_PIN_RESET);
    }
    else
    {
        __HAL_UART_DISABLE_IT(&huart_m, UART_IT_RXNE);
    }

    if(xTxEnable)
    {
        HAL_GPIO_WritePin(R_W_RS485_GPIO_Port,R_W_RS485_Pin,GPIO_PIN_SET);
        __HAL_UART_ENABLE_IT(&huart_m, UART_IT_TXE);
        __HAL_UART_DISABLE_IT(&huart_m, UART_IT_TC);
    }
    else
    {
        __HAL_UART_ENABLE_IT(&huart_m, UART_IT_TC);
        __HAL_UART_DISABLE_IT(&huart_m, UART_IT_TXE);

    }

}

BOOL
xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
    //return FALSE;
	switch (ucPORT)
  {
	case 0:
          huart_m.Instance = USART2;
          break;
 	case 1:
          huart_m.Instance = USART2;
          break;
	case 2:
          huart_m.Instance = USART2;
          break;
        default:
          return FALSE;
  }
  huart_m.Init.BaudRate = ulBaudRate;
  switch (ucDataBits)
  {
        case 8:
                huart_m.Init.WordLength = UART_WORDLENGTH_8B;
                break;
        default:
                return FALSE;
  }
  switch (eParity)
  {
    case MB_PAR_NONE:
            huart_m.Init.Parity = UART_PARITY_NONE;
            break;
    case MB_PAR_EVEN:
            huart_m.Init.Parity = UART_PARITY_EVEN;
            break;
    case MB_PAR_ODD:
            huart_m.Init.Parity = UART_PARITY_ODD;
            break;
    default:
            return FALSE;
  }
  huart_m.Init.StopBits = UART_STOPBITS_1;
  huart_m.Init.Mode = UART_MODE_TX_RX;
  huart_m.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart_m.Init.OverSampling = UART_OVERSAMPLING_16;
  return (HAL_OK == HAL_UART_Init(&huart_m));
}



BOOL
xMBPortSerialPutByte( CHAR ucByte )
{
    /* Put a byte in the UARTs transmit buffer. This function is called
     * by the protocol stack if pxMBFrameCBTransmitterEmpty( ) has been
     * called. */
    //return TRUE;
//    if(HAL_UART_Transmit(&huart_m,&ucByte, 1,10) != HAL_OK)
//        return FALSE;
//
//    return TRUE;

    huart_m.Instance->TDR=ucByte;
    return TRUE;
}

BOOL
xMBPortSerialGetByte( CHAR * pucByte )
{
    /* Return the byte in the UARTs receive buffer. This function is called
     * by the protocol stack after pxMBFrameCBByteReceived( ) has been called.
     */


//    if(HAL_UART_Receive(&huart_m,pucByte,1,10) != HAL_OK)
//       return FALSE;
//
//
//    return TRUE;
//    if(huart_m.Init.Parity == UART_PARITY_NONE)
//    {
//        *pucByte = (uint8_t)(huart_m.Instance->RDR & (uint8_t)0x00FF);
//    }
//    else
//    {
//        *pucByte = (uint8_t)(huart_m.Instance->RDR & (uint8_t)0x007F);
//    }
    *pucByte = (uint8_t)    huart_m.Instance->RDR;
    return TRUE;
}


