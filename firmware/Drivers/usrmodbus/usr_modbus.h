#ifndef __USR_MODBUS_H
#define __USR_MODBUS_H

#include "stm32l4xx_hal.h"
#include <stdio.h>

//  Номер регистра	Адрес регистра HEX	    Тип	            Название	                Тип
//   1-9999	           0000 до 270E	    Чтение-запись	Discrete Output Coils	         DO
//  10001-19999	       0000 до 270E	    Чтение	        Discrete Input Contacts	         DI
//  30001-39999	       0000 до 270E	    Чтение	        Analog Input Registers	         AI
//  40001-49999	       0000 до 270E	    Чтение-запись	Analog Output Holding Registers	 AO




//Код функции	            Что делает функция	                Тип значения	   Тип доступа
//01 (0x01)	      Чтение DO	         Read Coil Status	          Дискретное	    Чтение
//02 (0x02)	      Чтение DI	         Read Input Status	          Дискретное	    Чтение
//03 (0x03)	      Чтение AO	         Read Holding Registers       16 битное	        Чтение
//04 (0x04)	      Чтение AI	         Read Input Registers	      16 битное	        Чтение
//05 (0x05)	    Запись одного DO     Force Single Coil	          Дискретное	    Запись
//06 (0x06)	    Запись одного AO     Preset Single Register	      16 битное	        Запись
//15 (0x0F)	    Запись нескольких DO Force Multiple Coils	      Дискретное	    Запись
//16 (0x10)	    Запись нескольких AO Preset Multiple Registers	  16 битное	        Запись

/* -----------------------Slave Defines -------------------------------------*/
#define S_DISCRETE_INPUT_START        0 //DI регистр состояния источнмка
#define S_DISCRETE_INPUT_NDISCRETES   0

#define S_COIL_START                  17  //DO  регитср управления источником питания
#define S_COIL_NCOILS                 1

#define S_REG_INPUT_START             16    //AI регистр состояния
#define S_REG_INPUT_NREGS             1

#define S_REG_HOLDING_START           18   //AO уставка тока уставка напряжения
#define S_REG_HOLDING_NREGS           2

/* salve mode: holding register's all address */
#define          S_HD_RESERVE                     0
#define          S_HD_CPU_USAGE_MAJOR             1
#define          S_HD_CPU_USAGE_MINOR             2
/* salve mode: input register's all address */
#define          S_IN_RESERVE                     0
/* salve mode: coil's all address */
#define          S_CO_RESERVE                     0
/* salve mode: discrete's all address */
#define          S_DI_RESERVE                     0


void __critical_enter(void);
void __critical_exit(void);

void DINAR_UART_IRQHandler(UART_HandleTypeDef *huart);

void user_mb_write_state_reg(uint8_t data);
int user_mb_power_is_on(void);
uint16_t user_mb_read_current(void);
uint16_t user_mb_read_voltage(void);

#endif
