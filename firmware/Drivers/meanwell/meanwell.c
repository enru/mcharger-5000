#include "stm32l4xx_hal.h"
#include "meanwell.h"
#include "main.h"
#include "mdac.h"

void meanwell_remote(remote_state_t state)
{
    if( state == on )
        HAL_GPIO_WritePin(REM_CTRL_GPIO_Port,REM_CTRL_Pin,GPIO_PIN_SET);
    else if(state == off )
        HAL_GPIO_WritePin(REM_CTRL_GPIO_Port,REM_CTRL_Pin,GPIO_PIN_RESET);
}


bool meanwell_get_signal_state(meanwell_signals_t signal)
{
    switch (signal)
    {
        case sig_ac_fail :
            return HAL_GPIO_ReadPin(SIG_AC_FAIL2_GPIO_Port,SIG_AC_FAIL2_Pin) == GPIO_PIN_RESET;
        case sig_fan_fail:
            return HAL_GPIO_ReadPin(SIG_AC_FAIL2_GPIO_Port,SIG_AC_FAIL2_Pin) == GPIO_PIN_RESET;
        case sig_otp:
            return HAL_GPIO_ReadPin(SIG_OTP2_GPIO_Port,SIG_OTP2_Pin) == GPIO_PIN_RESET;
        case sig_dc_ok:
            return HAL_GPIO_ReadPin(SIG_DC_OK2_GPIO_Port,SIG_DC_OK2_Pin) == GPIO_PIN_RESET;
        default:
            break;
    }
}

float convert_current_to_output(float current)
{
    return MAX_GAIN_OUTPUT_VOLTAGE * current / MEANWELL_MAX_CURRENT;
}

float convert_voltage_to_output(float voltage)
{
    return MAX_GAIN_OUTPUT_VOLTAGE * voltage / MEANWELL_MAX_VOLTAGE ;
}


void meanwell_set_current(float current)
{
    if(current > MEANWELL_MAX_CURRENT || current < MEANWELL_MIN_CURRENT)
        return;

    float eq_vol = convert_current_to_output(current);
    mdac_set_current_chanel(eq_vol);
}


void meanwell_set_voltage(float voltage)
{
    if(voltage > MEANWELL_MAX_VOLTAGE || voltage < MEANWELL_MIN_VOLTAGE)
        return;

    float eq_vol = convert_voltage_to_output(voltage);
    mdac_set_voltage_chanel(eq_vol);

}

void meanwell_set_zero_outs(void)
{
    mdac_set_current_chanel(0.0f);
    mdac_set_voltage_chanel(0.0f);
}