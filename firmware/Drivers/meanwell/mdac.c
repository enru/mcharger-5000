#include "mdac.h"
#include "stm32l4xx_hal.h"
#include "main.h"




extern DAC_HandleTypeDef hdac1;


void mdac_set_output(uint32_t chanel, uint16_t data)
{
    if(data > 4095)
        Error_Handler();

    if (HAL_DAC_SetValue(&hdac1,chanel,DAC_ALIGN_12B_R, data) != HAL_OK)
        Error_Handler();

    if (HAL_DAC_Start(&hdac1, chanel) != HAL_OK)
        Error_Handler();
}

uint16_t convert_volt_on_dac_output_to_dac_data(float voltage)
{
    return (uint16_t) (MAX_DAC_DATA * voltage / VCCA_REF_VOLTAGE);
}

float convert_volt_on_outgain_to_volt_on_dac_output(float voltage)
{
    return  (float) voltage * VCCA_REF_VOLTAGE / MAX_GAIN_OUTPUT_VOLTAGE;
}

void mdac_set_voltage_chanel(float voltage)
{
    float dac_out = convert_volt_on_outgain_to_volt_on_dac_output(voltage);
    uint16_t dac_data = convert_volt_on_dac_output_to_dac_data(dac_out);
    mdac_set_output(DAC_VOLTAGE_CHANEL,dac_data);
}

void mdac_set_current_chanel(float voltage)
{
    float dac_out = convert_volt_on_outgain_to_volt_on_dac_output(voltage);
    uint16_t dac_data = convert_volt_on_dac_output_to_dac_data(dac_out);
    mdac_set_output(DAC_CURRENT_CHANEL,dac_data);
}