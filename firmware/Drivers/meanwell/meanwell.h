#ifndef __MEANWELL_H_
#define __MEANWELL_H_

#include "stdbool.h"


#define MEANWELL_MAX_CURRENT 105.0f
#define MEANWELL_MIN_CURRENT 21.0f

#define MEANWELL_MAX_VOLTAGE 48.0f
#define MEANWELL_MIN_VOLTAGE 9.6f


typedef enum
{
    off,
    on
}remote_state_t;


typedef enum
{
    sig_fan_fail,
    sig_ac_fail,
    sig_otp,
    sig_dc_ok,

}meanwell_signals_t;


bool meanwell_get_signal_state(meanwell_signals_t signal);
void meanwell_remote(remote_state_t state);
void meanwell_set_current(float current);
void meanwell_set_voltage(float voltage);
void meanwell_set_zero_outs(void);

#endif