#include "mean_mb.h"
#include "mb.h"
#include "usr_modbus.h"
#include "meanwell.h"



void mean_mb_update_status (void)
{
    uint8_t status = 0;
    if(meanwell_get_signal_state(sig_ac_fail))
        status |=  0x01;

    if(meanwell_get_signal_state(sig_dc_ok))
        status |= 0x02;

    if(meanwell_get_signal_state(sig_fan_fail))
        status |=  0x04;

    if(meanwell_get_signal_state(sig_otp))
        status |=  0x08;

    user_mb_write_state_reg(status);
}

void mean_mb_control_update(void)
{
    remote_state_t cmd = off;

    if(user_mb_power_is_on())
        cmd = on;

    meanwell_remote(cmd);
}


void mean_mb_update_param(void)
{
    float voltage,current = 0;

    voltage = user_mb_read_voltage() / 100.0f;
    current = user_mb_read_current() / 100.0f;

    meanwell_set_current(current);
    meanwell_set_voltage(voltage);
}

void mean_mb_set_zero_out(void)
{
    meanwell_set_zero_outs();
}

void mean_mb_task(void)
{
    mean_mb_update_status();
    mean_mb_control_update();
    mean_mb_update_param();
}