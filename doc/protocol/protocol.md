## PROTOCOL CHARGER BOARD


UART 19200 8N1

<table>
  <tr>
    <th>name</th>
    <th>address</th>
    <th>Modbus<br>command</th>
    <th>comments</th>
  </tr>
  <tr>
    <td>state register</td>
    <td>0x10 <br> 16 </td>
    <td>0x04</td>
    <td>4-15) reserved<br>3) OTP - fail<br>2)FAN -fail<br>1) DC - OK <br>0)AC -fail</td>
  </tr>
  <tr>
    <td>control register</td>
    <td>0x11 <br> 17</td>
    <td>0x05</td>
    <td> ON/OFF<br>coil<br><br></td>
  </tr>
  <tr>
    <td>current</td>
    <td>0x12<br> 18</td>
    <td rowspan="2">0x03<br>0x06<br>0x10</td>
    <td>min 2100 = 21.0A<br>max 10500 = 105.0A</td>
  </tr>
  <tr>
    <td>voltage</td>
    <td>0x13<br> 19</td>
    <td>min 960 = 9.6V<br>max 4800 = 48.0V</td>
  </tr>
</table>


## EXAMPLE


##### Read state register 0x10 16

* `readed 0x55`

![cap](src/status_read.png)

##### ON/OFF control register 0x11 17

![cap2](src/enable_disable.png)

##### Set current/voltage  0x12/0x13  18/19

* `set 50A`

![cap3](src/current50A.png)

* `set 48V`

![cap4](src/voltage48V.png)





