## CHARGER BOARD

This board for connect MeanWell RST-5000 and e.t.c to ModBus RTU network with specified protocol 

[schematic.pdf](doc/pcb/schematic.pdf).   
[firmware (clion gcc)](firmware/).   
[hardware](hardware/).  
[protocol](doc/protocol/protocol.md).  

![](doc/photo/top.png)
![](doc/photo/bot.png)


## simulation

Board has double DAC chanel and two Low Pass Filter for set through analog input current and voltage on MeanWell RST5000 etc + GAIN on OAmp which can up 3.3V - 5V

![](hardware/simulation/filter_design.png)

![](hardware/simulation/AFR.png)

![](hardware/simulation/multisim.png)

## WARNING 

When you are change current or voltage your must control timedif ! NO MORE THEN 0.1V per sec or 0.1A per sec. 